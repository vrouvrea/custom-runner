#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <limits>
#include <cmath>

/* Transform a point X [X0, X1, X2, ..., Xn-1, Xn] to its stereographic projection
 * A point Y [X0 / Xn, X1 / Xn, X2 / Xn, ..., Xn-1 / Xn]
 */
int main(int argc, char *argv[]) {
  if (argc < 3)
    return EXIT_FAILURE;

  std::vector<double> point;
  double final = atof(argv[argc - 1]);
  std::cout << "final = " << final << '\n';
  if (std::fabs(final) <= std::numeric_limits<double>::epsilon())
    return EXIT_FAILURE;

  std::transform(&argv[1], &argv[argc - 1], std::back_inserter(point), [&final](char* c) { return atof(c) / final; });

  // print result
  for (auto val : point)
    std::cout << val << " ";
  std::cout << "\n";

  return EXIT_SUCCESS;
}